"use strict";

class Supplement {
    constructor(name){
        this.name = name;

        if(name == 'cheeze'){
            this.price = 10;
            this.calories = 20;
        }else  if(name == 'salat'){
            this.price = 20;
            this.calories = 5;
        }else  if(name == 'potato'){
            this.price = 15;
            this.calories = 10;
        }else  if(name == 'spice'){
            this.price = 15;
            this.calories = 0;
        }else  if(name == 'nez'){
            this.price = 20;
            this.calories = 5;
        }
    }
}

class Hamburger{
    constructor(size = 'small') {
        this.size = size;
        this.supplements = [];
        if(this.size == 'small') {
            this.price = 50;
            this.calories = 20;
        }
        else{
            this.price = 100;
            this.calories = 40;
        }
    }

    addSupplement(supplement){
        this.supplements.push(supplement);
    }

    removeSupplement(supplement){
        let index = 0;
        for(let s of this.supplements){
            if(s.name == supplement)
                 this.supplements.splice(index, 1);
             index += 1;
        }
    }

    getSize(){
        return this.size;
    }

    getStuffing(){
        for(let s of this.supplements){
            console.log(s.name);
        }
    }

    getPrice(){
        let price = 0;
        price += this.price;

        for(let s of this.supplements){
            price += s.price;
        }
        return price;
    }

    getCalories(){
        let cals = 0;
        cals += this.calories;

        for(let s of this.supplements){
            cals += s.calories;
        }
        return cals;
    }

}

const h = new Hamburger();

h.getSize();

h.addSupplement(new Supplement('cheeze'));
h.addSupplement(new Supplement('potato'));
h.addSupplement(new Supplement('nez'));

h.getStuffing();


h.removeSupplement('potato');

h.getStuffing();

console.log(h.getPrice());
console.log(h.getCalories());


