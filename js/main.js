'use strict';
const API = 'https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses';

// do not use fetch!! Only Promise!
// let getRequest = (url, cb) => {
//     let xhr = new XMLHttpRequest();
//     xhr.open('GET', url, true);
//     xhr.onreadystatechange = () => {
//         if (xhr.readyState === 4) {
//             if (xhr.status !== 200) {
//                 console.log('Error!');
//             } else {
//                 cb(xhr.responseText);
//             }
//         }
//     }
//     xhr.send();
// }

let getRequest = (url, data) => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status !== 200) {
                    reject(`${xhr.status} ${xhr.statusText}`);
                } else {
                    resolve(xhr.responseText);
                }
            }
        }
        xhr.send(data);
    });
}

// let postRequest = (url, data) => {
//     return new Promise((resolve, reject) => {
//         let xhr = new XMLHttpRequest();
//         xhr.open('POST', url, true);
//         xhr.onreadystatechange = () => {
//             if (xhr.readyState === 4) {
//                 if (xhr.status !== 200) {
//                     reject(`${xhr.status} ${xhr.statusText}`);
//                 } else {
//                     resolve(xhr.responseText);
//                 }
//             }
//         }
//         xhr.send(data);
//     });
// }


class ProductItem {
    constructor(product, img='https://via.placeholder.com/200x150') { // img = './img/img.jpg'
        this.product_name = product.product_name;
        this.price = product.price;
        this.product_id = product.id_product;
        this.img = img;
    }

    addToCartHandler(event){
        const product_id = event.currentTarget.getAttribute('data-item-id');
        new Cart().addProduct(product_id)
    }

    addEventListenerToAddCart(){
        let addbtns = document.querySelectorAll('button.buy-btn[data-item-id]');

        addbtns.forEach(button => {
            button.addEventListener('click', this.addToCartHandler);
        });
    }

    render() {
        return `<div class="product-item" data-item-id="${this.product_id}">
              <img src="${this.img}" alt="Some img">
              <div class="desc">
                  <h3>${this.product_name}</h3>
                  <p>${this.price} \u20bd</p>
                  <button data-item-id="${this.product_id}" class="buy-btn">Купить</button>
              </div>
          </div>`;
    }
}


class ProductList {
    #goods;
    #allProducts;

    constructor(container = '.products') {
        this.container = container;
        this.#goods = [];
        this.#allProducts = [];

        this.#fetchGoods();
        this.#render();
    }

    get goods() {
        return this.#goods;
    }

    set goods(value) {
        this.#goods = value;
    }

    getTotalSum(){
        let totalSum = 0;
        for (const product of this.#goods) {
            totalSum += product.price;
        }
        return totalSum;
    }

    #fetchGoods(category, page) {
        getRequest(`${API}/catalogData.json`, `{"id_category": ${category}, "page_number": ${page}`)
            .then((data) => {
                this.#goods = JSON.parse(data);
                this.#render();
                console.log(this.getTotalSum());
            }).catch((error) => {
                console.log(error);
            });
    }

    #render() {
        const block = document.querySelector(this.container);

        for (const product of this.#goods) {
            const productObject = new ProductItem(product);
            this.#allProducts.push(productObject);
            block.insertAdjacentHTML('beforeend', productObject.render());
            productObject.addEventListenerToAddCart();
        }
    }
}

class CartItem{
    constructor(container = '.basket', img = './img/basket.jpg') {
        this.img = img;
        this.container = container;

        this.#render();
    }

    #render(){
        const basket = document.querySelector(this.container);
        //todo
    }
}

class Cart{
    constructor(container = '.basket-items') {
        this.container = container;
        this.goodsinCart = [];
    }

    addProduct(product_id, quality){
        getRequest(`${API}/addToBasket.json`, `{"id_product": ${product_id}, "quality": ${quality}`)
            .then((data) => {
                console.log(data);

            }).catch((error) => {
                console.log(error);
             });
    }

    removeProduct(product_id){
        getRequest(`${API}/deleteFromBasket.json`, `{"id_product": ${product_id}}`)
            .then((data) => {
                console.log(data);

            }).catch((error) => {
                console.log(error);
        });
    }

}

new ProductList();
//
// new Cart().removeProduct(123);


