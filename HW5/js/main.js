const API = 'https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses';

const app = new Vue({
  el: '#app',
  data: {
    catalogUrl: '/catalogData.json',
    products: [],
    itemsInCart: [],
    searchLine: 'text for search',
    isVisibleCart: false,
    cartTotalSum: 0,
    imgCatalog: 'https://via.placeholder.com/200x150'
  },
  methods: {
    getJson(url){
      return fetch(url)
          .then(result => result.json())
          .catch(error => {
            console.log(error);
          })
    },
    addProduct(product){
      let exists = this.itemsInCart.find(el => el.id_product === product.id_product)
      if (exists) {
        exists.quantity++;
      } else {
        let prod = Object.assign({quantity: 1}, product);
        this.itemsInCart.push(prod)
      }

      this.cartTotalSum += product.price;
      this.isVisibleCart = true;
    },
    filterGoods(){
      const regex = new RegExp(this.searchLine, 'ig');

      this.products = this.products.filter(({product_name}) => product_name.match(regex));
    },
    removeItemCart(product){
      if(product.quantity > 1)
        product.quantity--;
      else
        this.itemsInCart.splice(this.itemsInCart.indexOf(product), 1);

      if(this.itemsInCart.length > 0){

      }else{
        this.cartTotalSum = 0;
        this.isVisibleCart = false;
      }
    }

  },
  computed: {
    style() {
      return {
        color: 'red',
        fontSize: '25px',
      };
    }
  },
  beforeCreated() {

  },
  created() {
    this.getJson(`${API + this.catalogUrl}`)
        .then(data => {
          this.products = data;
        });
  },
  beforeMount() {

  },
  mounted() {

  },
  beforeUpdate() {

  },
  updated() {

  },
  beforeDestroy() {

  },
  destroyed() {

  },
});
