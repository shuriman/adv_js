class Checker{
    constructor(selector) {
        this.patterns = {
            phone: /^\+7\(\d{3}\)\d{3}\-\d{4}$/,
            name: /^[а-яёa-z]+$/i,
            email: /^[\w.\-]+@\w+\.[a-z]{2,10}$/i
        };

        this.errors = {
            name: 'Имя содержит только буквы',
            phone: 'Телефон имеет вид +7(000)000-0000',
            email: 'E-mail имеет вид mymail@mail.ru, или my.mail@mail.ru, или my-mail@mail.ru'
        }

        this.invalid_class = 'invalid';
        this.error_class = 'error_class';
        this.form = document.querySelector(selector);
        this.isValid = false;
        this.#validateForm();
    }

    #validateForm() {
        let invalids = [...this.form.querySelectorAll('.'+this.invalid_class)].forEach(e => e.classList.remove(this.invalid_class))
        let errors = [...this.form.querySelectorAll('.'+this.error_class)].forEach(e => e.remove())
        let fields = [...this.form.querySelectorAll('.send__message__input')].forEach(e => this.#validate(e))

        if(![...this.form.querySelectorAll('.'+this.error_class)].length){
            this.isValid = true;
        }
    }

    #validate(input){
        if(this.patterns[input.id]){
            if(!this.patterns[input.id].test(input.value)){
                input.classList.add(this.invalid_class);
                this.#addErrorMsg(input);
            }
        }
    }

    #addErrorMsg(input){
        const error = `<div class="${this.error_class}">${this.errors[input.id]}</div> `;
        input.insertAdjacentHTML('afterend', error);
    }
}