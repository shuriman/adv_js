const API = 'https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/mastedddr/responses';

const app = new Vue({
    el: '#app',
    data: {

    },
    methods: {
        getJson(url){
            return fetch(url)
                .then(result => result.json())
                .catch(error => {
                    this.$refs.warning.setWarning(`Ошибка ${url}`);
                })
        },
    },
    mounted() {
        console.log(this);
    }
});

