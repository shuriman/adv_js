Vue.component('warning', {
    data(){
        return{
            showWarning: false,
            textWaring: ''
        }
    },

    methods: {
        setWarning(warning){
            this.textWaring = warning;
            this.showWarning = true;
        }
    },
    template: `
         <div>
            <div class="warning-block" v-show="showWarning">
                                {{ this.textWaring }}
            </div>
        </div>
    `
});