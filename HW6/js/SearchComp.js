Vue.component('search', {
    data(){
        return {
           
        }
    },
    methods: {

    },
    template:
        `<form action="#" class="search-form" @submit.prevent="filter">
            <input type="text" class="search-field" v-model="userSearch">
            <button class="btn-search" @click="$root.$refs.products.filter()" type="submit">
            <i class="fas fa-search"></i>
        </button>
        </form>`
});