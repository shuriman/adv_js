const fs = require('fs');
const moment = require('moment')
const cart = require('./cart');

const actions = {
  add: cart.add,
  change: cart.change,
  del: cart.del,
};

const log = 'stats.json';


const logger = (state, id) => {
  const m = moment().format('MM-DD-YYYY, hh:mm:ss');
  state = `${m} ${state} ${id}`
  console.log(state);


  fs.appendFile(log, state+'\n', (err) => {
    if (err) {
      console.log(err);
    } else {

    }
  })
}

const handler = (req, res, action, file) => {
  fs.readFile(file, 'utf-8', (err, data) => {
    if (err) {
      res.sendStatus(404, JSON.stringify({result: 0, text: err}));
    } else {
      if(action == 'add')
        logger(action, req.body.id_product);
      else
        logger(action, req.params.id);

      const newCart = actions[action](JSON.parse(data), req);
      fs.writeFile(file, newCart, (err) => {
        if (err) {
          res.send('{"result": 0}');
        } else {
          res.send('{"result": 1}');
        }
      })
    }
  });
};

module.exports = handler;
